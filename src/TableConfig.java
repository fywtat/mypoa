import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import crawler.ArticlesService;
import crawler.KeyWord;
import crawler.SearchHistory;
import crawler.SearchHistory;
import crawler.classes.Article;

/**
 * 
 * <b>Description:</b><br/>
 * 
 * <br/>
 * <b>CreatedTime:</b>2016年3月7日 下午3:28:20<br/>
 * <br/>
 * <b>Environment:</b> <br/>
 * 
 * @author xinhui.peng(pxinhui)
 */

public class TableConfig {

	public final static String[] title_third = {"帖子标题", "URL", "日期", "情感倾向"};
	public final static String[] title_forth = {"编号", "词条内容", "创建时间", "更新时间", "操作"};
	public final static String[] title_fifth = {"搜索词条", "搜索时间", "搜索数据条数", "操作"};

	public final static int PAGE_SIZE = 40;
	public final static int MAX_PAGE_SIZE = 10;

	public static List<Vector<String>> getArticle() {
		return getArticle("");
	}

	public static List<Vector<String>> getArticle(String key_word) {
		List<Article> list = ArticlesService.queryArticles(key_word);
		List<Vector<String>> result = new ArrayList<Vector<String>>();
		for (int i = 0; i < list.size(); i++) {
			Vector<String> v = new Vector<String>();
			Article item = list.get(i);
			v.add(item.title);
			v.add(item.url);
			v.add(item.create_time);
			v.add(item.qgqx);
			v.add(item.full_content);
			v.add(item.qgqd);
			v.add(item.key_word);
			result.add(v);
		}
		return result;
	}
	public static List<Vector<String>> getKeyword() {
		List<KeyWord> list = ArticlesService.queryKeywords();
		List<Vector<String>> result = new ArrayList<Vector<String>>();
		for (int i = 0; i < list.size(); i++) {
			Vector<String> v = new Vector<String>();
			KeyWord item = list.get(i);
			v.add(i + 1 + "");
			v.add(item.name);
			v.add(item.create_time);
			v.add(item.update_time);
			v.add("");
			result.add(v);
		}
		return result;
	}
	public static List<Vector<String>> getSearchHistory() {
		List<SearchHistory> list = ArticlesService.querySearchHistory();
		List<Vector<String>> result = new ArrayList<Vector<String>>();
		for (int i = 0; i < list.size(); i++) {
			Vector<String> v = new Vector<String>();
			SearchHistory item = list.get(i);
			v.add(item.name);
			v.add(item.search_time);
			v.add(item.search_count);
			v.add("");
			result.add(v);
		}
		return result;
	}
	public static JTableCreator.TableOptItem[] getForthOpt(JTableCreator.OnTableClickListener update, JTableCreator.OnTableClickListener delete) {
		
		JTableCreator.TableOptItem[] array = new JTableCreator.TableOptItem[2];
		array[0] = new JTableCreator.TableOptItem("更新", update);
		array[1] = new JTableCreator.TableOptItem("删除", delete);
		return array;
	}
	public static JTableCreator.TableOptItem[] getFifthOpt(JTableCreator.OnTableClickListener first) {
		
		JTableCreator.TableOptItem[] array = new JTableCreator.TableOptItem[1];
		array[0] = new JTableCreator.TableOptItem("查看", first);
		return array;
	}
	public static void showDetail() {

	}

	public static JTableCreator createTableThrid(JScrollPane scrollPanel, JPanel panel) {
		final DialogArticleDetail detail = new DialogArticleDetail();
		JTableCreator.OnTableSelectListener selectListener = new JTableCreator.OnTableSelectListener() {
			@Override
			public void onTableSelect(int allPositon, MouseEvent event) {
				System.out.println("all:" + allPositon);
				// JOptionPane.showConfirmDialog(scrollPanel, "关键字不要为空，亲！",
				// "警告", JOptionPane.DEFAULT_OPTION);
				detail.setData(TableConfig.getArticle().get(allPositon));
			}
		};
		int[] minWidth = {-1, -1, 150, -1};
		int[] maxWidth = {-1, -1, 300, 200};
		List<Vector<String>> list = null;// TableConfig.getArticle();
		return TableConfig.setTable(scrollPanel, panel, TableConfig.title_third, maxWidth, minWidth, list,
				selectListener, null);
	}
	public static JTableCreator createTableFourth(JScrollPane scrollPanel, JPanel panel,
			JTableCreator.OnTableClickListener update, JTableCreator.OnTableClickListener delete) {
		JTableCreator.OnTableSelectListener selectListener = new JTableCreator.OnTableSelectListener() {
			@Override
			public void onTableSelect(int allPositon, MouseEvent event) {
				System.out.println("all:" + allPositon);
			}
		};
		JTableCreator.TableOptItem[] opt = TableConfig.getForthOpt(update,delete);
		int[] minWidth = {40, -1, 150, 150, -1};
		int[] maxWidth = {40, -1, 300, 300, 300};
		List<Vector<String>> list = null;// TableConfig.getKeyword();
		return TableConfig.setTable(scrollPanel, panel, TableConfig.title_forth, maxWidth, minWidth, list,
				selectListener, opt);

	}
	public static JTableCreator createTableFifTh(JScrollPane scrollPanel, JPanel panel,
			JTableCreator.OnTableClickListener listener) {
		JTableCreator.OnTableSelectListener selectListener = new JTableCreator.OnTableSelectListener() {
			@Override
			public void onTableSelect(int allPositon, MouseEvent event) {
				System.out.println("all:" + allPositon);
			}
		};
		JTableCreator.TableOptItem[] opt = TableConfig.getFifthOpt(listener);
		int[] minWidth = {-1, 150, 150, -1};
		int[] maxWidth = {-1, 300, 300, 300};
		List<Vector<String>> list = null;// TableConfig.getSearchHistory();
		return TableConfig.setTable(scrollPanel, panel, TableConfig.title_fifth, maxWidth, minWidth, list,
				selectListener, opt);
	}
	public static void setTableThird(JTableCreator creator,String keyword) {
		setTableData(getArticle(keyword), creator);
	}
	public static void setTableFourth(JTableCreator creator) {
		setTableData(getKeyword(), creator);
	}
	public static void setTableFifth(JTableCreator creator) {
		setTableData(getSearchHistory(), creator);
	}
	/**
	 * 添加表
	 * 
	 * @param scrollPanel
	 * @param panel
	 * @param title
	 * @param data
	 * @param selectListener
	 * @param optItem
	 */
	public static JTableCreator setTable(JScrollPane scrollPanel, JPanel panel, String[] title, int[] maxWidth,
			int[] minWidth, List<Vector<String>> data, JTableCreator.OnTableSelectListener selectListener,
			JTableCreator.TableOptItem[] optItem) {
		JTableCreator creator = new JTableCreator();
		scrollPanel.setViewportView(creator.creatTable(title, maxWidth, minWidth, selectListener, optItem));
		JTableCreator.OnPageChangeListener listener = new JTableCreator.OnPageChangeListener() {
			@Override
			public void onPageChange(int index, int pageValue) {
				System.err.println("ee:" + index + "--：" + pageValue);
			}
		};

		int pageSize = TableConfig.PAGE_SIZE;
		int page = 1;
		int allSize = 0;
		if (data != null) {
			allSize = data.size();
			page = allSize / pageSize + (allSize % pageSize == 0 ? 0 : 1);
		}
		System.out.println("all:" + allSize + "-page:" + page);
		creator.createPage(panel, 1, page, TableConfig.MAX_PAGE_SIZE, listener);
		creator.setTableData(data, pageSize);
		return creator;
	}
	private static void setTableData(List<Vector<String>> data, JTableCreator creator) {
		int pageSize = TableConfig.PAGE_SIZE;
		int page = 1;
		int allSize = 0;
		if (data != null) {
			allSize = data.size();
			page = allSize / pageSize + (allSize % pageSize == 0 ? 0 : 1);
		}
		System.out.println("all:" + allSize + "-page:" + page);
		creator.setPage(1, page, TableConfig.MAX_PAGE_SIZE);
		creator.setTableData(data, pageSize);
	}
}
