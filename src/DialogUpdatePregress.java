import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cn.edu.hfut.dmic.webcollector.crawler.OnCrawlerVisitListener;
import crawler.CrawlerBlog;

/**
 * 
 * <b>Description:</b><br/>
 * 
 * <br/>
 * <b>CreatedTime:</b>2016年3月15日 上午11:18:26<br/>
 * <br/>
 * <b>Environment:</b> <br/>
 * 
 * @author xinhui.peng(pxinhui)
 */

public class DialogUpdatePregress extends JFrame {

	public DialogUpdatePregress() {
		super();
		initCompanent(700, 200);

	}

	JLabel updateTime;
	JLabel updateCount;
	private String title = "舆情更新开始！";
	private String UPDATE_TIME = "耗时：%s";
	private String UPDATE_COUNT = "获取到的新内容数目：%s条";
	void initCompanent(int fwidth, int height) {
		int width = fwidth - 20;
		setLayout(new BorderLayout());
		JPanel textPanel = new JPanel();
		BoxLayout box = new BoxLayout(textPanel, BoxLayout.Y_AXIS);
		textPanel.setLayout(box);
		JLabel titleLable = new JLabel();
		titleLable.setText(title);
		textPanel.add(titleLable);

		String time = "0s";
		updateTime = new JLabel();
		updateTime.setText(String.format(UPDATE_TIME, time));
		textPanel.add(updateTime);

		int count = 0;
		updateCount = new JLabel();
		updateCount.setText(String.format(UPDATE_COUNT, count));
		textPanel.add(updateCount);

		add(textPanel, BorderLayout.CENTER);
		JButton button = new JButton("关闭");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				showOnClose();
				super.mouseClicked(e);
			}
		});
		JPanel bpanel = new JPanel();
		bpanel.setLayout(new FlowLayout());
		bpanel.add(button);
		add(bpanel, BorderLayout.SOUTH);
		setTitle("更新词条");
		setSize(new Dimension(fwidth, height));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				showOnClose();
			}
		});
		setLocationRelativeTo(null);// 居中
		setResizable(false);
		setVisible(true);
	}
	CrawlerBlog crawlerBlog;
	int size = 0;
	boolean complete = false;
	void updateKeyword(String keyword) {
		if (crawlerBlog != null) {
			crawlerBlog.stop();
			crawlerBlog = null;
		}
		setVisible(true);

		crawlerBlog = new CrawlerBlog("./tmp", keyword, true);
		crawlerBlog.next_link_regex = ".*page=\\d+.*";
		crawlerBlog.current_urls = new ArrayList<String>();
		final long current = System.currentTimeMillis();
		crawlerBlog.setOnCrawlerVisitListener(new OnCrawlerVisitListener() {
			@Override
			public int onVisit(String s) {

				return 0;
			}
			@Override
			public int onComplete(int count) {
				complete = true;
				return 0;
			}

			@Override
			public int onUpdate(int updateSize) {
				size += updateSize;
				showUpdate(toTimeStr(System.currentTimeMillis() - current), size);
				return 0;
			}
		});
		Timer timer = new Timer();

		timer.schedule(new TimerTask() {

			@Override
			public void run() {

				showUpdate(toTimeStr(System.currentTimeMillis() - current), size);
			}
		}, 1000);
		crawlerBlog.setThreads(2);
		try {
			crawlerBlog.start(2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	String toTimeStr(long time) {
		long t = time / 1000;
		int miao = (int) (t % 60);
		t = t / 60;
		int fen = (int) (t % 60);
		t = t / 60;
		StringBuffer sb = new StringBuffer();
		if (t > 0L) {
			sb.append(miao + "时");
		}
		if (fen > 0) {
			sb.append(miao + "分");
		}
		return sb.append(miao + "秒").toString();

	}
	private void showOnClose() {
		int result = DialogToolUtil.showUpdateConfirm(null);
		System.out.println(result);
		if (complete) {
			setVisible(false);
			return;
		}
		if (result == 0) {// TODO 是
			setVisible(false);
			if (crawlerBlog != null) {
				crawlerBlog.stop();
				crawlerBlog = null;
			}

		} else if (result == 1) {// 否

		}
	}
	public void showUpdate(String time, int count) {
		// setVisible(true);
		updateTime.setText(String.format(UPDATE_TIME, time));
		updateCount.setText(String.format(UPDATE_COUNT, count));
	}
	public static void main(String[] args) {
		// new DialogUpdatePregress().showUpdate("0s", 0);
	}
}
