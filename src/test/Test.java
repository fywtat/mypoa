package test;/*
 * Created by JFormDesigner on Tue Feb 16 11:36:24 CST 2016
 */


import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author wang yu
 */
public class Test extends JFrame {
    public static void main(String[] args) {
        //System.out.print(1);
//        Test t=new Test();
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        String text = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">";
        int start = text.indexOf("charset=");
        System.out.println(start);
        String temp = text.substring(start);
        System.out.println(temp);
        int end = temp.indexOf("\"");
        System.out.println(end);
        System.out.println(temp.substring(0+"charset=".length(), end));
        
    }
    public Test() {
//        initComponents();
//        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void startCrawl(ActionEvent e) {
        if(e.getSource().equals(button1)){
            //System.out.print(123);
//            JDialog jDialog=new JDialog(this,"测试",true);
//            jDialog.setSize(new Dimension(500,400));
//            jDialog.setVisible(true);
            //当前内容
            String text=textArea2.getText();
            textArea2.setText(text+"\n"+"待爬取url列表:");
        }

    }
    public void selectWord(ActionEvent e){
        if(e.getSource().equals(button3)){
            //跳转到选择词条tab页
            tabbedPane1.setSelectedIndex(3);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - wang yu
        tabbedPane1 = new JTabbedPane();
        panel1 = new JPanel();
        label2 = new JLabel();
        textField1 = new JTextField();
        button1 = new JButton();
        button3 = new JButton();
        button2 = new JButton();
        label3 = new JLabel();
        scrollPane1 = new JScrollPane();
        textArea2 = new JTextArea();
        panel2 = new JPanel();
        panel21 = new JPanel();
        panel211 = new JPanel();
        panel22 = new JPanel();
        panel221 = new JPanel();
        panel23 = new JPanel();
        panel24 = new JPanel();
        label4 = new JLabel();
        label5 = new JLabel();
        button4 = new JButton();
        label6 = new JLabel();
        label7 = new JLabel();
        label8 = new JLabel();
        label9 = new JLabel();
        label10 = new JLabel();
        label11 = new JLabel();
        label12 = new JLabel();
        label13 = new JLabel();
        label14 = new JLabel();
        label15 = new JLabel();
        label16 = new JLabel();
        label17 = new JLabel();
        label18 = new JLabel();
        panel3 = new JPanel();
        panel6 = new JPanel();
        label19 = new JLabel();
        label20 = new JLabel();
        label21 = new JLabel();
        label22 = new JLabel();
        label23 = new JLabel();
        label24 = new JLabel();
        button5 = new JButton();
        scrollPane2 = new JScrollPane();
        table1 = new JTable();
        panel4 = new JPanel();
        scrollPane3 = new JScrollPane();
        table2 = new JTable();
        panel5 = new JPanel();
        scrollPane4 = new JScrollPane();
        table3 = new JTable();

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startCrawl(e);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectWord(e);
            }
        });
        //======== tabbedPane1 ========
        {
            tabbedPane1.setPreferredSize(new Dimension(800, 600));

            //======== panel1 ========
            {

                // JFormDesigner evaluation mark
                panel1.setBorder(new javax.swing.border.CompoundBorder(
                    new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                        "", javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                        java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

                panel1.setLayout(new TableLayout(new double[][] {
                    {0.01, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL},
                    {0.01, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}}));

                //---- label2 ----
                label2.setText("\u641c\u7d22\u65b0\u95fb\u4e3b\u9898\u8bcd\u6761");
                panel1.add(label2, new TableLayoutConstraints(0, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                panel1.add(textField1, new TableLayoutConstraints(0, 2, 2, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                //---- button1 ----
                button1.setText("\u5f00\u59cb");

                panel1.add(button1, new TableLayoutConstraints(3, 2, 4, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                //---- button3 ----
                button3.setText("\u9009\u62e9\u8bcd\u6761");
                panel1.add(button3, new TableLayoutConstraints(5, 2, 6, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                //---- button2 ----
                button2.setText("\u505c\u6b62");
                panel1.add(button2, new TableLayoutConstraints(8, 2, 8, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                //---- label3 ----
                label3.setText("\u5f53\u524d\u722c\u53d6\u6570\u636e\uff1a");
                panel1.add(label3, new TableLayoutConstraints(0, 4, 1, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                //======== scrollPane1 ========
                {
                    //textArea2.setSize(new Dimension(600,500));
                    textArea2.setPreferredSize(new Dimension(600,500));
                    scrollPane1.setViewportView(textArea2);
                    scrollPane1.createVerticalScrollBar();
                    //scrollPane1.setSize(new Dimension(600,500));
                }
                panel1.add(scrollPane1, new TableLayoutConstraints(0, 6, 11, 13, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            }
            tabbedPane1.addTab("\u8206\u60c5\u641c\u96c6", panel1);

            //舆情分析
            //======== panel2 ========
            {
//                panel2.setLayout(new TableLayout(new double[][] {
//                    {122, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED},
//                    {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}}));
//
//                //---- label4 ----
//                label4.setText("\u5206\u6790\u7ed3\u679c\uff1a");
//                panel2.add(label4, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.CENTER));
//
//                //---- label5 ----
//                label5.setText("\u641c\u7d22\u5173\u952e\u8bcd\uff1a");
//                panel2.add(label5, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- button4 ----
//                button4.setText("\u5bfc\u51fa\u62a5\u544a");
//                panel2.add(button4, new TableLayoutConstraints(10, 1, 14, 1, TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));
//
//                //---- label6 ----
//                label6.setText("text");
//                panel2.add(label6, new TableLayoutConstraints(0, 2, 0, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label7 ----
//                label7.setText("\u5185\u5bb9\u63d0\u53d6\u603b\u91cf");
//                panel2.add(label7, new TableLayoutConstraints(0, 3, 0, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label8 ----
//                label8.setText("text");
//                panel2.add(label8, new TableLayoutConstraints(0, 4, 0, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label9 ----
//                label9.setText("\u4eca\u65e5\u5185\u5bb9");
//                panel2.add(label9, new TableLayoutConstraints(0, 5, 0, 5, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label10 ----
//                label10.setText("text");
//                panel2.add(label10, new TableLayoutConstraints(0, 6, 0, 6, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label11 ----
//                label11.setText("\u672c\u5468\u5185\u5bb9");
//                panel2.add(label11, new TableLayoutConstraints(0, 7, 0, 7, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label12 ----
//                label12.setText("text");
//                panel2.add(label12, new TableLayoutConstraints(0, 9, 0, 9, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label13 ----
//                label13.setText("\u6b63\u9762\u5185\u5bb9");
//                panel2.add(label13, new TableLayoutConstraints(0, 10, 0, 10, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label14 ----
//                label14.setText("text");
//                panel2.add(label14, new TableLayoutConstraints(0, 11, 0, 11, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label15 ----
//                label15.setText("\u8d1f\u9762\u5185\u5bb9");
//                panel2.add(label15, new TableLayoutConstraints(0, 12, 0, 12, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label16 ----
//                label16.setText("text");
//                panel2.add(label16, new TableLayoutConstraints(0, 13, 0, 13, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label17 ----
//                label17.setText("\u4e2d\u6027\u5185\u5bb9");
//                panel2.add(label17, new TableLayoutConstraints(0, 14, 0, 14, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//                //---- label18 ----
//                label18.setText("\u63d0\u53d6\u5173\u952e\u8bcd\uff1a");
//                panel2.add(label18, new TableLayoutConstraints(0, 18, 0, 18, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
//
//
//                panel2.add(new JButton("ceshi "), new TableLayoutConstraints(10, 20, 10, 20, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

               //不用 tableloyout 用grid布局

               panel2.setLayout(new GridLayout(4,1));
                //panel21
                //关键词
                label5.setText("搜索关键词:");
                button4.setText("导出报告");
                panel21.add(label5);
                panel21.add(button4);

                //panel22
                label6.setText("数值1");
                label7.setText("内容提取总量");
                label8.setText("数值2");
                label9.setText("今日内容");
                label10.setText("数值3");
                label11.setText("本周内容");

                //添加图表
                //饼图1
                JFreeChart chart=createChart(createDataset());
                ChartPanel localChartPanel=new ChartPanel(chart);
                //BufferedImage image =chart.createBufferedImage(800, 300,BufferedImage.TYPE_INT_RGB, null);
                //localChartPanel.setSize(new Dimension(100,100));
                localChartPanel.setPreferredSize(new Dimension(200,200));
                panel211.add(localChartPanel);


                panel22.add(label6,BorderLayout.WEST);
                panel22.add(label7,BorderLayout.WEST);
                panel22.add(label8,BorderLayout.WEST);
                panel22.add(label9,BorderLayout.WEST);
                panel22.add(label10,BorderLayout.WEST);
                panel22.add(label11,BorderLayout.WEST);
                panel22.add(panel211,BorderLayout.EAST);

                //panel23

                label12.setText("数值1");
                label13.setText("正面内容");
                label14.setText("数值2");
                label15.setText("负面内容");
                label16.setText("数值3");
                label17.setText("中性内容");

                panel23.add(panel221,BorderLayout.EAST);
                panel23.add(label12,BorderLayout.WEST);
                panel23.add(label13,BorderLayout.WEST);
                panel23.add(label14,BorderLayout.WEST);
                panel23.add(label15,BorderLayout.WEST);
                panel23.add(label16,BorderLayout.WEST);
                panel23.add(label17,BorderLayout.WEST);

                //panel24
                label18.setText("提取关键词");
                panel24.add(label18);

                panel2.add(panel21);
                panel2.add(panel22);
                panel2.add(panel23);
                panel2.add(panel24);



            }
            tabbedPane1.addTab("\u8206\u60c5\u5206\u6790", panel2);

            //======== panel3 ========
            {
                panel3.setLayout(new BorderLayout());

                //======== panel6 ========
                {
                    panel6.setLayout(new FlowLayout());

                    //---- label19 ----
                    label19.setText("text");
                    panel6.add(label19);

                    //---- label20 ----
                    label20.setText("\u5185\u5bb9\u63d0\u53d6\u603b\u91cf");
                    panel6.add(label20);

                    //---- label21 ----
                    label21.setText("text");
                    panel6.add(label21);

                    //---- label22 ----
                    label22.setText("\u4eca\u65e5\u5185\u5bb9");
                    panel6.add(label22);

                    //---- label23 ----
                    label23.setText("text");
                    panel6.add(label23);

                    //---- label24 ----
                    label24.setText("\u672c\u5468\u5185\u5bb9");
                    panel6.add(label24);

                    //---- button5 ----
                    button5.setText("\u5bfc\u51fa\u62a5\u544a");
                    panel6.add(button5);
                }
                panel3.add(panel6, BorderLayout.NORTH);

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(table1);
                }
                panel3.add(scrollPane2, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("\u6587\u7ae0\u5206\u6790", panel3);

            //======== panel4 ========
            {
                panel4.setLayout(new BorderLayout());

                //======== scrollPane3 ========
                {
                    scrollPane3.setViewportView(table2);
                }
                panel4.add(scrollPane3, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("\u8bcd\u6761\u7ba1\u7406", panel4);

            //======== panel5 ========
            {
                panel5.setLayout(new BorderLayout());

                //======== scrollPane4 ========
                {
                    scrollPane4.setViewportView(table3);
                }
                panel5.add(scrollPane4, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("\u641c\u7d22\u5386\u53f2", panel5);
        }
        //tabbedPane1.setVisible(true);
        this.setTitle("舆情分析系统");
        this.add(tabbedPane1);
        //this.setPreferredSize(new Dimension(800, 600));
        this.setSize(new Dimension(800, 600));
        this.setLocationRelativeTo(null);
        this.setVisible(true);


        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - wang yu
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JLabel label2;
    private JTextField textField1;
    private JButton button1;
    private JButton button3;
    private JButton button2;
    private JLabel label3;
    private JScrollPane scrollPane1;
    private JTextArea textArea2;
    private JPanel panel2;
    private JPanel panel21;
    private JPanel panel211;
    private JPanel panel22;
    private JPanel panel221;
    private JPanel panel23;
    private JPanel panel24;
    private JLabel label4;
    private JLabel label5;
    private JButton button4;
    private JLabel label6;
    private JLabel label7;
    private JLabel label8;
    private JLabel label9;
    private JLabel label10;
    private JLabel label11;
    private JLabel label12;
    private JLabel label13;
    private JLabel label14;
    private JLabel label15;
    private JLabel label16;
    private JLabel label17;
    private JLabel label18;
    private JPanel panel3;
    private JPanel panel6;
    private JLabel label19;
    private JLabel label20;
    private JLabel label21;
    private JLabel label22;
    private JLabel label23;
    private JLabel label24;
    private JButton button5;
    private JScrollPane scrollPane2;
    private JTable table1;
    private JPanel panel4;
    private JScrollPane scrollPane3;
    private JTable table2;
    private JPanel panel5;
    private JScrollPane scrollPane4;
    private JTable table3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    private static PieDataset createDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("One", new Double(43.2));
        dataset.setValue("Two", new Double(10.0));
        dataset.setValue("Three", new Double(27.5));
        dataset.setValue("Four", new Double(17.5));
        dataset.setValue("Five", new Double(11.0));
        dataset.setValue("Six", new Double(19.4));
        return dataset;
    }
    /**
     * Creates a chart.
     *
     * @param dataset  the dataset.
     *
     * @return A chart.
     */
    private static JFreeChart createChart(PieDataset dataset) {

        JFreeChart chart = ChartFactory.createPieChart(
                "Pie Chart Demo 1",  // chart title
                dataset,             // data
                true,               // include legend
                true,
                false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setSectionOutlinesVisible(false);
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);

        return chart;

    }
}
