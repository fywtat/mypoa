import cn.edu.hfut.dmic.webcollector.crawler.OnCrawlerVisitListener;
import crawler.CrawlerSouGou;
import crawler.CrawlerWeibo;
import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import crawler.ArticlesService;
import crawler.CrawlerBlog;

/**
 * @author wang yu
 */
public class Index extends JFrame {
    public static void main(String[] args) {
        // System.out.print(1);
        new Index();
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    public Index() {
        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void buttonAction(ActionEvent e) {

//        if(true){
//            textArea2.setText(e+"\n"+textArea2.getText());
//            return;
//        }
        // 判断关键字是否为空
        if (e.getSource().equals(button1)) {
            if (textField1.getText().matches("\\s*")) {
                // 弹出模式框
                JOptionPane.showConfirmDialog(this, "关键字不要为空，亲！", "警告", JOptionPane.DEFAULT_OPTION);

            } else {
                // 开始按钮变得不可点击
                button1.setEnabled(false);
                // 一个线程来采集，初始化采集标示，方便停止按钮使用
                startCaiJi();

            }
        } else if (e.getSource().equals(button2)) {
            // 停掉采集线程
            crawlerBlog.stop();
            crawlerWeibo.stop();
            crawlerSouGou.stop();
            // 停止按钮的功能
            button1.setEnabled(true);
        } else if (e.getSource().equals(button4)) {
            //导出报告
            System.out.println("button4");
            selectWord(label5.getText());
        }
    }

    void startCaiJi() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                //博客爬虫
                crawlerBlog = new CrawlerBlog("./tmp", textField1.getText(), false);
                crawlerBlog.setOnCrawlerVisitListener(new OnCrawlerVisitListener() {
                    @Override
                    public int onVisit(String s) {
//						textArea2.append(s + "\n");
                        textArea2.setText(s + "\n" + textArea2.getText());
                        return 0;
                    }

                    @Override
                    public int onComplete(int count) {
                        setComplete(count);
                        return 0;
                    }

                    @Override
                    public int onUpdate(int updateSize) {
                        return 0;
                    }
                });
                crawlerBlog.setThreads(5);
                try {
                    crawlerBlog.start(30);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
              //微博爬虫
                crawlerWeibo = new CrawlerWeibo("./tmp", textField1.getText(), false);
                crawlerWeibo.setOnCrawlerVisitListener(new OnCrawlerVisitListener() {
                    @Override
                    public int onVisit(String s) {
//						textArea2.append(s + "\n");
                        textArea2.setText(s + "\n" + textArea2.getText());
                        return 0;
                    }

                    @Override
                    public int onComplete(int count) {
                        setComplete(count);
                        return 0;
                    }

                    @Override
                    public int onUpdate(int updateSize) {
                        return 0;
                    }
                });
                crawlerWeibo.setThreads(5);
                try {
                    crawlerWeibo.start(30);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                //搜狗爬虫
                crawlerSouGou = new CrawlerSouGou("./tmp", textField1.getText(), false);
                crawlerSouGou.setOnCrawlerVisitListener(new OnCrawlerVisitListener() {
                    @Override
                    public int onVisit(String s) {
//						textArea2.append(s + "\n");
                        textArea2.setText(s + "\n" + textArea2.getText());
                        return 0;
                    }

                    @Override
                    public int onComplete(int count) {
                        setComplete(count);
                        return 0;
                    }

                    @Override
                    public int onUpdate(int updateSize) {
                        return 0;
                    }
                });
                crawlerSouGou.setThreads(5);
                try {
                    crawlerSouGou.start(30);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        }).start();
    }

    int total_count = 0;
    int crawler_num = 0;

    void setComplete(int count) {
        total_count += count;
        crawler_num += 1;
        if (crawler_num == CRAWLER_NUMS) {
            button1.setEnabled(true);
            DialogToolUtil.showSearchComplete(this, total_count);
        }
    }

    public void selectWord(ActionEvent e) {
        if (e.getSource().equals(button3)) {
            // 跳转到选择词条tab页
            tabbedPane1.setSelectedIndex(3);
        }
    }

    public void selectWord(String keyword) {
        // 跳转到选择词条tab页
         tabbedPane1.setSelectedIndex(2);
        // 设置文章列表


    }

    private void initComponents() {
        tabbedPane1 = new JTabbedPane();
        panel1 = new JPanel();
        label2 = new JLabel();
        textField1 = new JTextField();
        button1 = new JButton();
        button3 = new JButton();
        button2 = new JButton();
        label3 = new JLabel();
        scrollPane1 = new JScrollPane();
        textArea2 = new JTextArea();
        textArea2.setLineWrap(true);
        panel2 = new JPanel();
        panel21 = new JPanel();
        panel211 = new JPanel();
        panel22 = new JPanel();
        panel221 = new JPanel();
        panel222 = new JPanel();
        panel23 = new JPanel();
        panel231 = new JPanel();
        panel232 = new JPanel();
        panel24 = new JPanel();
        panel2Center = new JPanel();
        label4 = new JLabel();
        label5 = new JLabel();
        label_keyword = new JLabel();
        button4 = new JButton();
        label6 = new JLabel();
        label7 = new JLabel();
        label8 = new JLabel();
        label9 = new JLabel();
        label10 = new JLabel();
        label11 = new JLabel();
        label12 = new JLabel();
        label13 = new JLabel();
        label14 = new JLabel();
        label15 = new JLabel();
        label16 = new JLabel();
        label17 = new JLabel();
        label18 = new JLabel();
        panel3 = new JPanel();
        panel6 = new JPanel();
        label19 = new JLabel();
        label20 = new JLabel();
        label21 = new JLabel();
        label22 = new JLabel();
        label23 = new JLabel();
        label24 = new JLabel();
        button5 = new JButton();
        scrollPane2 = new JScrollPane();
        table1 = new JTable();
        panel4 = new JPanel();
        scrollPane3 = new JScrollPane();
        table2 = new JTable();
        panel5 = new JPanel();
        scrollPane4 = new JScrollPane();
        table3 = new JTable();

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction(e);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectWord(e);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction(e);
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction(e);
            }
        });

        // ======== tabbedPane1 ========
        {
            tabbedPane1.setPreferredSize(new Dimension(800, 600));

            // ======== panel1 ========
            {

                // JFormDesigner evaluation mark
                panel1.setBorder(new javax.swing.border.CompoundBorder(new javax.swing.border.TitledBorder(
                        new javax.swing.border.EmptyBorder(0, 0, 0, 0), "", javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.BOTTOM, new Font("Dialog", Font.BOLD, 12), Color.red), panel1
                        .getBorder()));
                panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
                    public void propertyChange(java.beans.PropertyChangeEvent e) {
                        if ("border".equals(e.getPropertyName()))
                            throw new RuntimeException();
                    }
                });

                panel1.setLayout(new TableLayout(new double[][]{
                        {0.01, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.FILL},
                        {0.01, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                                TableLayout.PREFERRED}}));

                // ---- label2 ----
                label2.setText("搜索新闻主题词条");
                panel1.add(label2, new TableLayoutConstraints(0, 1, 1, 1, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));
                panel1.add(textField1, new TableLayoutConstraints(0, 2, 2, 2, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));

                // ---- button1 ----
                button1.setText("开始");

                panel1.add(button1, new TableLayoutConstraints(3, 2, 4, 2, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));

                // ---- button3 ----
                button3.setText("选择词条");
                panel1.add(button3, new TableLayoutConstraints(5, 2, 6, 2, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));

                // ---- button2 ----
                button2.setText("停止");
                panel1.add(button2, new TableLayoutConstraints(8, 2, 8, 2, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));

                // ---- label3 ----
                label3.setText("当前爬取数据");
                panel1.add(label3, new TableLayoutConstraints(0, 4, 1, 4, TableLayoutConstraints.FULL,
                        TableLayoutConstraints.FULL));

                // ======== scrollPane1 ========
                {
                    // textArea2.setSize(new Dimension(600,500));
                    //textArea2.setPreferredSize(new Dimension(600, 500));
                    scrollPane1.setViewportView(textArea2);
                    scrollPane1.createVerticalScrollBar();

                    // scrollPane1.setSize(new Dimension(600,500));
                }
//				panel1.add(scrollPane1, new TableLayoutConstraints(0, 6, 11, 13, TableLayoutConstraints.FULL,
//						TableLayoutConstraints.FULL));
                panelFirst = new JPanel();
                panelFirst.setLayout(new BorderLayout());
                panelFirst.add(panel1, BorderLayout.NORTH);
                panelFirst.add(scrollPane1, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("舆情搜集", panelFirst);

            // 舆情分析
            // ======== panel2 ========
            {
                // panel2.setLayout(new TableLayout(new double[][] {
                // {122, TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED},
                // {TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED,
                // TableLayout.PREFERRED, TableLayout.PREFERRED}}));
                //
                // //---- label4 ----
                // label4.setText("\u5206\u6790\u7ed3\u679c\uff1a");
                // panel2.add(label4, new TableLayoutConstraints(0, 0, 0, 0,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.CENTER));
                //
                // //---- label5 ----
                // label5.setText("\u641c\u7d22\u5173\u952e\u8bcd\uff1a");
                // panel2.add(label5, new TableLayoutConstraints(0, 1, 0, 1,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- button4 ----
                // button4.setText("\u5bfc\u51fa\u62a5\u544a");
                // panel2.add(button4, new TableLayoutConstraints(10, 1, 14, 1,
                // TableLayoutConstraints.RIGHT, TableLayoutConstraints.FULL));
                //
                // //---- label6 ----
                // label6.setText("text");
                // panel2.add(label6, new TableLayoutConstraints(0, 2, 0, 2,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label7 ----
                // label7.setText("\u5185\u5bb9\u63d0\u53d6\u603b\u91cf");
                // panel2.add(label7, new TableLayoutConstraints(0, 3, 0, 3,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label8 ----
                // label8.setText("text");
                // panel2.add(label8, new TableLayoutConstraints(0, 4, 0, 4,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label9 ----
                // label9.setText("\u4eca\u65e5\u5185\u5bb9");
                // panel2.add(label9, new TableLayoutConstraints(0, 5, 0, 5,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label10 ----
                // label10.setText("text");
                // panel2.add(label10, new TableLayoutConstraints(0, 6, 0, 6,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label11 ----
                // label11.setText("\u672c\u5468\u5185\u5bb9");
                // panel2.add(label11, new TableLayoutConstraints(0, 7, 0, 7,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label12 ----
                // label12.setText("text");
                // panel2.add(label12, new TableLayoutConstraints(0, 9, 0, 9,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label13 ----
                // label13.setText("\u6b63\u9762\u5185\u5bb9");
                // panel2.add(label13, new TableLayoutConstraints(0, 10, 0, 10,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label14 ----
                // label14.setText("text");
                // panel2.add(label14, new TableLayoutConstraints(0, 11, 0, 11,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label15 ----
                // label15.setText("\u8d1f\u9762\u5185\u5bb9");
                // panel2.add(label15, new TableLayoutConstraints(0, 12, 0, 12,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label16 ----
                // label16.setText("text");
                // panel2.add(label16, new TableLayoutConstraints(0, 13, 0, 13,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label17 ----
                // label17.setText("\u4e2d\u6027\u5185\u5bb9");
                // panel2.add(label17, new TableLayoutConstraints(0, 14, 0, 14,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                // //---- label18 ----
                // label18.setText("\u63d0\u53d6\u5173\u952e\u8bcd\uff1a");
                // panel2.add(label18, new TableLayoutConstraints(0, 18, 0, 18,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
                //
                //
                // panel2.add(new JButton("ceshi "), new
                // TableLayoutConstraints(10, 20, 10, 20,
                // TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

                // 不用 tableloyout 用grid布局

                // panel2.setLayout(new GridLayout(4, 1));
                // panel21
                panel2.setLayout(new BorderLayout());
                // 关键词
                label5.setText("搜索关键词:");
                label_keyword.setText("");
                button4.setText("显示文章列表");
                panel21.add(label5);
                panel21.add(label_keyword);
                panel21.add(button4);

                // panel22
                label6.setText("数值1");
                label7.setText("内容总量");
                label8.setText("数值2");
                label9.setText("今日数量");
                label10.setText("数值3");
                label11.setText("本周数量");
                GridLayout gridlayout = new GridLayout(6, 1);
                // gridlayout.setHgap(30);
                gridlayout.setVgap(25);
                panel221.setLayout(gridlayout);
                panel221.add(label6);
                panel221.add(label7);
                panel221.add(label8);
                panel221.add(label9);
                panel221.add(label10);
                panel221.add(label11);

                barPanel1 = new MyBarChartPanel("", "日期", "数量", new Dimension(800, 300), new HashMap<String, Object>() {
                    {
//                        put("20160311", 111);
//                        put("20160312", 222);
//                        put("20160313", 150);
//                        put("20160314", 100);
//                        put("20160315", 200);
//                        put("20160316", 500);
                    }
                });
                panel222.add(barPanel1);
                panel22.add(panel221);
                panel22.add(panel222);

                // panel23

                panel231.setLayout(gridlayout);
                label12.setText("数值1");
                label13.setText("正面内容");
                label14.setText("数值2");
                label15.setText("负面内容");
                label16.setText("数值3");
                label17.setText("中性内容");
                panel231.add(label12);
                panel231.add(label13);
                panel231.add(label14);
                panel231.add(label15);
                panel231.add(label16);
                panel231.add(label17);

                piePanel1 = new MyPieChartPanel("", new Dimension(300, 300), new HashMap<String, Object>() {
                    {
//                        put("正面", 1);
//                        put("负面", 2);
//                        put("中性", 3);
                    }
                });
                panel232.add(piePanel1);

                barPanel2 = new MyBarChartPanel("", "日期", "数量", new Dimension(400, 300), new HashMap<String, Object>() {
                    {
//                        put("20160311", 111);
//                        put("20160312", 222);
//                        put("20160313", 150);
                    }
                });
                panel232.add(barPanel2);

                panel23.add(panel231, BorderLayout.WEST);
                panel23.add(panel232, BorderLayout.CENTER);

                // panel24
                label18.setText("提取关键词");
                panel24.add(label18);

                panel2Center.setLayout(new GridLayout(2, 1));
                panel2Center.add(panel22);
                panel2Center.add(panel23);
                panel2.add(panel21, BorderLayout.NORTH);
                panel2.add(panel2Center, BorderLayout.CENTER);
                panel2.add(panel24, BorderLayout.SOUTH);

            }
            tabbedPane1.addTab("舆情分析", panel2);

            // ======== panel3 ========
            {
                panel3.setLayout(new BorderLayout());

                // ======== panel6 ========
                {
                    panel6.setLayout(new FlowLayout());

                    // ---- label19 ----
                    label19.setText("text");
                    panel6.add(label19);

                    // ---- label20 ----
                    label20.setText("\u5185\u5bb9\u63d0\u53d6\u603b\u91cf");
                    panel6.add(label20);

                    // ---- label21 ----
                    label21.setText("text");
                    panel6.add(label21);

                    // ---- label22 ----
                    label22.setText("\u4eca\u65e5\u5185\u5bb9");
                    panel6.add(label22);

                    // ---- label23 ----
                    label23.setText("text");
                    panel6.add(label23);

                    // ---- label24 ----
                    label24.setText("\u672c\u5468\u5185\u5bb9");
                    panel6.add(label24);

                    // ---- button5 ----
                    button5.setText("\u5bfc\u51fa\u62a5\u544a");
                    panel6.add(button5);

                }
                panel3.add(panel6, BorderLayout.NORTH);

                // ======== scrollPane2 ========
                {

                    creatorThird = TableConfig.createTableThrid(scrollPane2, panel3);// Table
                    // 3
                }
                panel3.add(scrollPane2, BorderLayout.CENTER);

            }
            tabbedPane1.addTab("文章分析", panel3);

            // ======== panel4 ========
            {
                panel4.setLayout(new BorderLayout());

                // ======== scrollPane3 ========

                {

                    creatorFourth = TableConfig.createTableFourth(scrollPane3, panel4, update, delete);// Table
                    // 4
                }
                panel4.add(scrollPane3, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("词条管理", panel4);

            // ======== panel5 ========
            {
                panel5.setLayout(new BorderLayout());

                // ======== scrollPane4 ========
                {

                    creatorFifth = TableConfig.createTableFifTh(scrollPane4, panel5, tableFifthListener);// Table
                    // 5
                }
                panel5.add(scrollPane4, BorderLayout.CENTER);
            }
            tabbedPane1.addTab("搜索历史", panel5);
        }
        // tabbedPane1.setVisible(true);
        this.setTitle("舆情分析系统");
        tabbedPane1.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                int selectIndex = tabbedPane1.getSelectedIndex();
                switch (selectIndex) {
                    case 1:// TODO

                        setYuQingFenXi(keyword, ArticlesService.getKeyWordData(keyword), ArticlesService.getWeekData(keyword));
                        break;
                    case 2:
                        Map<String, String> map = ArticlesService.getKeyWordData(label_keyword.getText());
                        label19.setText(map.get("total"));
                        label21.setText(map.get("today"));
                        label23.setText(map.get("week"));
                        TableConfig.setTableThird(creatorThird,label_keyword.getText());
                        break;
                    case 3:
                        TableConfig.setTableFourth(creatorFourth);
                        break;
                    case 4:
                        TableConfig.setTableFifth(creatorFifth);
                        break;

                    default:
                        break;
                }
            }
        });
        this.add(tabbedPane1);
        // this.setPreferredSize(new Dimension(800, 600));
        this.setSize(new Dimension(1000, 800));
        this.setLocationRelativeTo(null);// 居中
        this.setVisible(true);
    }

    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel panelFirst;
    private JLabel label2;
    private JTextField textField1;
    private JButton button1;
    private JButton button3;
    private JButton button2;
    private JLabel label3;
    private JScrollPane scrollPane1;
    private JTextArea textArea2;
    private JPanel panel2;
    private JPanel panel21;
    private JPanel panel211;
    private JPanel panel22;
    private JPanel panel221;
    private JPanel panel222;
    private JPanel panel23;
    private JPanel panel231;
    private JPanel panel232;
    private JPanel panel24;
    private JPanel panel2Center;
    private JLabel label4;
    private JLabel label5;
    private JLabel label_keyword;
    private JButton button4;
    private JLabel label6;
    private JLabel label7;
    private JLabel label8;
    private JLabel label9;
    private JLabel label10;
    private JLabel label11;
    private JLabel label12;
    private JLabel label13;
    private JLabel label14;
    private JLabel label15;
    private JLabel label16;
    private JLabel label17;
    private JLabel label18;
    private JPanel panel3;
    private JPanel panel6;
    private JLabel label19;
    private JLabel label20;
    private JLabel label21;
    private JLabel label22;
    private JLabel label23;
    private JLabel label24;
    private JButton button5;
    private JScrollPane scrollPane2;
    private JTable table1;
    private JPanel panel4;
    private JScrollPane scrollPane3;
    private JTable table2;
    private JPanel panel5;
    private JScrollPane scrollPane4;
    private JTable table3;
    private MyBarChartPanel barPanel1;
    private MyBarChartPanel barPanel2;
    private MyPieChartPanel piePanel1;

    private int CRAWLER_NUMS = 3;

    // 博客爬虫
    private CrawlerBlog crawlerBlog;
    //微博爬虫
    private CrawlerWeibo crawlerWeibo;
    //搜狗爬虫
    private CrawlerSouGou crawlerSouGou;

    // 设置舆情分析面板数据
    private void setYuQingFenXi(String keyword, Map<String, String> map, Map<String, Object> bar1Map) {
        // 设置关键字
        label_keyword.setText(keyword);
        // 设置内容量数据
        label6.setText(map.get("total"));
        // label7.setText("内容提取总量");
        label8.setText(map.get("today"));
        // label9.setText("今日内容");
        label10.setText(map.get("week"));
        // label11.setText("本周内容");
        // 设置bar图1
        barPanel1 = new MyBarChartPanel("", "日期", "数量", new Dimension(800, 300), bar1Map);
        panel222.remove(0);
        panel222.add(barPanel1, 0);
        // 设置情感倾向数据
        label12.setText(map.get("zm"));
        // label13.setText("正面内容");
        label14.setText(map.get("fm"));
        // label15.setText("负面内容");
        label16.setText(map.get("zx"));
        // label17.setText("中性内容");
        // 设置饼图
        Map<String, Object> hashmap = new HashMap<String, Object>();
        hashmap.put("正面", map.get("zm"));
        hashmap.put("负面", map.get("fm"));
        hashmap.put("中性", map.get("zx"));

        piePanel1 = new MyPieChartPanel("", new Dimension(300, 300), hashmap);
        // 重绘chart
        // todo 可考虑其他方法
        panel232.remove(0);
        panel232.add(piePanel1, 0);
        // piePanel1.repaint();
        // panel232.add(piePanel1);
        // piePanel1.repaint();
        // panel232.repaint();
        // panel2.repaint();
        // this.repaint();
        // 设置bar图2
        // todo 需要处理
        Map<String, Object> hashmap2 = new HashMap<String, Object>();
        hashmap2.put("正面", map.get("zm"));
        hashmap2.put("负面", map.get("fm"));
        hashmap2.put("中性", map.get("zx"));
        barPanel2 = new MyBarChartPanel("", "情感倾向", "数量", new Dimension(400, 300), hashmap2);
        panel232.remove(1);
        panel232.add(barPanel2, 1);

    }

    String keyword = "";
    JTableCreator.OnTableClickListener tableFifthListener = new JTableCreator.OnTableClickListener() {
        @Override
        public void onTableClick(int optIndex, int allPositon, MouseEvent event) {
            System.out.println("opt:" + optIndex + "-all:" + allPositon);
            Vector<String> vecotr = creatorFifth.getTableData().get(allPositon);
            keyword = vecotr.get(0);
            tabbedPane1.setSelectedIndex(1);
        }
    };
    JTableCreator.OnTableClickListener update = new JTableCreator.OnTableClickListener() {
        DialogUpdatePregress update;

        @Override
        public void onTableClick(int optIndex, int allPositon, MouseEvent event) {
            System.out.println("opt:" + optIndex + "-all:" + allPositon);
            Vector<String> vecotr = creatorFourth.getTableData().get(allPositon);
            final String key = vecotr.get(1);
            System.out.println("key:" + key);
            if (update == null) {
                update = new DialogUpdatePregress();
            }
            new Thread(new Runnable() {

                @Override
                public void run() {
                    update.updateKeyword(key);

                }
            }).start();

        }
    };
    JTableCreator.OnTableClickListener delete = new JTableCreator.OnTableClickListener() {
        @Override
        public void onTableClick(int optIndex, int allPositon, MouseEvent event) {
            System.out.println("opt:" + optIndex + "-all:" + allPositon);
            Vector<String> vecotr = creatorFourth.getTableData().get(allPositon);
            String key = vecotr.get(1);
            System.out.println("key:" + key);
            ArticlesService.deleteKeyword(key);
        }
    };
    JTableCreator creatorThird;
    JTableCreator creatorFourth;
    JTableCreator creatorFifth;
}
