import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.awt.*;
import java.util.Map;

/**
 * Created by Administrator on 2016/3/3.
 * 根据数据和标题大小来初始化饼图
 */
public class MyPieChartPanel extends ChartPanel{
    public MyPieChartPanel(String title, Dimension dimension,Map<String,Object> data) {
        super(createChart(title,createDataset(data)));
        this.setSize(dimension);
        this.setPreferredSize(dimension);
    }


    public static PieDataset createDataset(Map<String,Object> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        for (String key : data.keySet()) {
            Object value = data.get(key);
            dataset.setValue(key, Double.parseDouble(value.toString()));
           // System.out.println("Key = " + key + ", Value = " + value);

        }
        //dataset.setValue("One", new Double(43.2));
//        dataset.setValue("Two", new Double(10.0));
//        dataset.setValue("Three", new Double(27.5));
//        dataset.setValue("Four", new Double(17.5));
//        dataset.setValue("Five", new Double(11.0));
//        dataset.setValue("Six", new Double(19.4));
        return dataset;
    }

    public static JFreeChart createChart(String title,PieDataset dataset) {

        JFreeChart chart = ChartFactory.createPieChart(title,
                dataset, // data
                false, // include legend
                true, false);

        PiePlot plot = (PiePlot) chart.getPlot();

        plot.setSectionOutlinesVisible(false);
        plot.setLabelFont(new Font("微软雅黑", Font.PLAIN, 10));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);
        return chart;

    }


}
