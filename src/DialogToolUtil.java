import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JOptionPane;

public class DialogToolUtil {

	private static int showMessage(Component parentComponent, String title, String message, int optionType) {
		// JOptionPane.;
		int messageType = JOptionPane.INFORMATION_MESSAGE;
		Icon icon = null;
		// JOptionPane.showConfirmDialog(parentComponent, message, title,
		// optionType, messageType);

		Object[] options = null;
		Object initialValue = null;
		return JOptionPane.showOptionDialog(parentComponent, message, title, optionType, messageType, icon, options,
				initialValue);
	}

	public static void showSearchComplete(Component parentComponent,int count) {
		showMessage(parentComponent,"搜集结果", String.format("搜集过程结束\n本次共搜集%s条数据", count), JOptionPane.CLOSED_OPTION);
	}
	public static int showUpdateConfirm(Component parentComponent) {
		return showMessage(parentComponent,"结束更新提示", "更新尚未完成，是否结束？", JOptionPane.YES_NO_OPTION);

	}
	public static void main(String[] args) {
		showUpdateConfirm(null);
	}
}
