import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * 
 * <b>Description:</b><br/>
 * 
 * <br/>
 * <b>CreatedTime:</b>2016年3月14日 上午11:31:58<br/>
 * <br/>
 * <b>Environment:</b> <br/>
 * 
 * @author xinhui.peng(pxinhui)
 */

public class DialogArticleDetail extends JFrame {

	public DialogArticleDetail() {
		super();
		initCompanent(750, 500);

	}

	JTextField keyword;
	JTextField title;
	JTextField time;
	JTextField from;
	JTextField qg;
	JTextField qgqd;
	JTextField temp;
	JTextArea content;
	void initCompanent(int fwidth, int height) {
		int width = fwidth - 20;
		JPanel all = new JPanel();
		BoxLayout boxY = new BoxLayout(all, BoxLayout.Y_AXIS);
		all.setLayout(new FlowLayout());

		// s
		JPanel line = new JPanel();
		BoxLayout box = new BoxLayout(line, BoxLayout.X_AXIS);
		line.setLayout(box);

		JPanel item = new JPanel();
		boxY = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(boxY);

		JLabel label = new JLabel();
		label.setText("舆论标题");
		item.add(label);
		title = new JTextField();
		item.add(title);

		item.setPreferredSize(new Dimension(width * 2 / 3, 40));
		line.add(item);

		item = new JPanel();
		boxY = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(boxY);

		label = new JLabel();
		label.setText("舆论时间");
		item.add(label);
		time = new JTextField();
		// text.setPreferredSize(new Dimension(width / 3, 40));
		item.add(time);
		item.setPreferredSize(new Dimension(width / 3, 40));

		line.add(item);
		all.add(line);// e
		// s
		line = new JPanel();
		box = new BoxLayout(line, BoxLayout.X_AXIS);
		line.setLayout(box);

		item = new JPanel();
		box = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(box);

		label = new JLabel();
		label.setText("舆论来源");
		item.add(label);
		from = new JTextField();
		item.add(from);
		item.setPreferredSize(new Dimension(width * 2 / 3, 40));
		line.add(item);

		item = new JPanel();
		box = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(box);
		label = new JLabel();
		label.setText("情感倾向");
		item.add(label);
		qg = new JTextField();
		item.add(qg);
		item.setPreferredSize(new Dimension(width  / 3, 40));
		line.add(item);

		all.add(line);// e

		line = new JPanel();
		box = new BoxLayout(line, BoxLayout.X_AXIS);
		line.setLayout(box);

		item = new JPanel();
		box = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(box);

		label = new JLabel();
		label.setText("情感强度");
		item.add(label);
		qgqd = new JTextField();
		item.add(qgqd);
		item.setPreferredSize(new Dimension(width * 2 / 3, 40));
		line.add(item);

		item = new JPanel();
		box = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(box);

		//关键词
		label = new JLabel();
		label.setText("关键词");
		item.add(label);
		keyword = new JTextField();
		item.add(keyword);
		item.setPreferredSize(new Dimension(width  / 3, 40));
		line.add(item);
		all.add(line);// e

		// s
		line = new JPanel();
		box = new BoxLayout(line, BoxLayout.X_AXIS);
		line.setLayout(box);

		item = new JPanel();
		box = new BoxLayout(item, BoxLayout.Y_AXIS);
		item.setLayout(box);

		label = new JLabel();
		label.setText("舆论内容");
		item.add(label);

		content = new JTextArea();
		content.setLineWrap(true);
		item.add(content);
		item.setPreferredSize(new Dimension(width, height));
		line.add(item);

		all.add(line);// end

		setSize(new Dimension(fwidth, height));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		add(all);
		setTitle("舆论详情");
		setLocationRelativeTo(null);// 居中
		setResizable(false);
	}
	public void setData(Vector<String> vector) {
		this.setVisible(true);
		title.setText(vector.get(0));
		time.setText(vector.get(2));
		from.setText(vector.get(1));
		qg.setText(vector.get(3));
		content.setText(vector.get(4));
		qgqd.setText(vector.get(5));
		keyword.setText(vector.get(6));
	}

	public static void main(String[] args) {
		new DialogArticleDetail().setVisible(true);
	}
}
