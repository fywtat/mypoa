/**
 * 
 * <b>Description:</b><br/>
 * 
 * <br/>
 * <b>CreatedTime:</b>2016年3月14日 上午10:32:21<br/>
 * <br/>
 * <b>Environment:</b> <br/>
 * 
 * @author xinhui.peng(pxinhui)
 */

package crawler;

public class KeyWord {

	public String name;
	public String create_time;
	public String update_time;
	public KeyWord(String name, String create_time, String update_time) {
		super();
		this.name = name;
		this.create_time = create_time;
		this.update_time = update_time;
	}

}
