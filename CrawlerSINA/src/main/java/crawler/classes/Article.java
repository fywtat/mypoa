package crawler.classes;

/**
 * Created by Administrator on 2015/7/4.
 */
public class Article {
    public String type;//文章类型 微博，博客
    public String key_word;
    public String url;
    public String title;
    public String content;
    public String full_content;
    public String src_html;
    public String full_html;
    public String create_time;
    public String qgqx;
    public String qgqd;



    public Article(String type,String key_word, String url, String title, String content, String src_html,
                   String create_time,String full_content,String full_html,String qgqx,String qgqd) {
        this.type = type;
        this.key_word=key_word;
        this.url = url;
        this.title = title;
        this.content = content;
        this.src_html = src_html;
        this.create_time = create_time;
        this.full_content=full_content;
        this.full_html=full_html;
        this.qgqx=qgqx;
        this.qgqd=qgqd;
    }
    public Article(String type,String key_word, String url, String title, String content, String src_html,
                   String create_time,String full_content,String full_html) {
        this.type = type;
        this.key_word=key_word;
        this.url = url;
        this.title = title;
        this.content = content;
        this.src_html = src_html;
        this.create_time = create_time;
        this.full_content=full_content;
        this.full_html=full_html;
        this.qgqx=qgqx;
        this.qgqd=qgqd;
    }


    @Override
    public String toString() {
        return "Article{" +
                "type='" + type + '\'' +
                ", key_word='" + key_word + '\'' +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", full_content='" + full_content + '\'' +
                ", src_html='" + src_html + '\'' +
                ", full_html='" + full_html + '\'' +
                ", create_time='" + create_time + '\'' +
                ", qgqx='" + qgqx + '\'' +
                ", qgqd='" + qgqd + '\'' +
                '}';
    }

    public  void printInfo()
    {
        System.out.println("type:"+this.type);
        System.out.println("url:"+this.url);
        System.out.println("title:"+this.title);
        System.out.println("contnet:"+this.content);
        //System.out.println("shop_name:"+this.src_html);
        System.out.println("create_time:"+this.create_time);


    }


}

