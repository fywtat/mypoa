package crawler.util;

/**
 * Created by Administrator on 2015/7/5.
 */
public class StringUtils {
    //count the substring
    public static Integer subStringCount(String str, String subStr) {
        Integer c = 0;
        Integer pos = 0;
        String tmp = "";
        while (pos != -1) {
            //当前串
            tmp = str.substring(pos);
            if (tmp.indexOf(subStr) != -1) {
                c++;
                pos += tmp.indexOf(subStr) + subStr.length();
            } else {
                break;
            }
        }
        return c;
    }

    public static String[] splitAndFilter(String str, String divide, String filter) {
        String[] strarr = str.split(divide);
        //计算符合filter的数量
        Integer fc = 0;
        for (int i = 0; i < strarr.length; i++) {
            if (strarr[i].matches(filter)) {
                fc++;
            }
        }
        String[] res = new String[strarr.length - fc];
        //剔除不需要的字符 参数为正则表达式
        Integer j = 0;
        for (int i = 0; i < strarr.length; i++) {
            if (!strarr[i].matches(filter)) {
                res[j] = strarr[i];
                j++;
            }
        }


        return res;

    }

    public static String decodeUnicode(String theString) {

        char aChar;

        int len = theString.length();

        StringBuffer outBuffer = new StringBuffer(len);

        for (int x = 0; x < len; ) {

            aChar = theString.charAt(x++);

            if (aChar == '\\') {

                aChar = theString.charAt(x++);

                if (aChar == 'u') {

                    // Read the xxxx

                    int value = 0;

                    for (int i = 0; i < 4; i++) {

                        aChar = theString.charAt(x++);

                        switch (aChar) {

                            case '0':

                            case '1':

                            case '2':

                            case '3':

                            case '4':

                            case '5':

                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';

                    else if (aChar == 'n')

                        aChar = '\n';

                    else if (aChar == 'f')

                        aChar = '\f';

                    outBuffer.append(aChar);

                }

            } else

                outBuffer.append(aChar);

        }

        return outBuffer.toString();

    }

    public static void main(String[] args) {
//        System.out.println(StringUtils.subStringCount("javajava","java1"));
//        System.out.println(StringUtils.subStringCount("起批量 3-4 件 5-9 件 ≥10 件","件"));
//        System.out.println(StringUtils.subStringCount(" 价格 ¥ 180.00 ¥ 175.00 ¥ 170.00","¥"));
//
        System.out.println(StringUtils.splitAndFilter("   1   2  3  ", " ", " *"));


    }
}
