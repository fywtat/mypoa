package crawler.util;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//



import java.util.Iterator;
import java.util.Set;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Weibo {
    public Weibo() {
    }

    public static String getSinaCookie(String username, String password) throws Exception {
        StringBuilder sb = new StringBuilder();
        HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.CHROME);
        driver.setJavascriptEnabled(true);
        driver.get("http://weibo.com/");
        WebElement mobile = driver.findElementByCssSelector("#pl_login_form > div.login_switch > div:nth-child(2) > div.info_list.username > div > input");
        mobile.sendKeys(new CharSequence[]{username});
        WebElement pass = driver.findElementByCssSelector("#pl_login_form > div.login_switch > div:nth-child(2) > div.info_list.password > div > input");
        pass.sendKeys(new CharSequence[]{password});
        //WebElement rem = driver.findElementByCssSelector("#pl_login_form > div.login_switch > div:nth-child(2) > div.info_list.login_btn > a > span");
        //rem.click();
        WebElement submit = driver.findElementByCssSelector("#pl_login_form > div.login_switch > div:nth-child(2) > div.info_list.login_btn > a > span");
        submit.click();
        Set cookieSet = driver.manage().getCookies();
        driver.close();
        Iterator result = cookieSet.iterator();

        while(result.hasNext()) {
            Cookie cookie = (Cookie)result.next();
            sb.append(cookie.getName() + "=" + cookie.getValue() + ";");
        }

        String result1 = sb.toString();
        if(result1.contains("gsid_CTandWM")) {
            return result1;
        } else {
            throw new Exception("weibo login failed");
        }
    }
}
