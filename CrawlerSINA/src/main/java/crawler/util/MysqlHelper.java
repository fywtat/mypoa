package crawler.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Administrator on 2015/7/6.
 */
public  class MysqlHelper {
    public  Properties prop;
    public  Connection getConn()  {
        //获取配置信息
        prop = new Properties();
        try {
            //打印当前目录
            //System.out.println(System.getProperty("user.dir"));
            //System.out.println(Class.class.getClass().getResource("/").getPath());
            //System.out.println(this.getClass().getPackage());

            File f=new File("db.properties");
            System.out.println(f.getAbsolutePath());

            prop.load(new FileInputStream("db.properties"));
            //prop.load(this.getClass().getClassLoader().getResourceAsStream("crawler/util/db.properties"));


        } catch (IOException e) {
            e.printStackTrace();
        }

        String driver=prop.getProperty("mysqldriver");
        String url=prop.getProperty("mysqlurl");
        String user=prop.getProperty("mysqluser");
        String passwd=prop.getProperty("mysqlpasswd");

        //获取数据库的链接
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conn= null;
        try {
            conn = DriverManager.getConnection(url, user, passwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void main(String[] args) {
        MysqlHelper mysqlHelper=new MysqlHelper();
        Connection conn=mysqlHelper.getConn();
    }
}
