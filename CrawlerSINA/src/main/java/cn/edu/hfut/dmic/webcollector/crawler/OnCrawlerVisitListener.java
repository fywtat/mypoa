package cn.edu.hfut.dmic.webcollector.crawler;

/**
 * Created by Administrator on 2016/3/15.
 */
public interface OnCrawlerVisitListener {
    public int onVisit(String url);
    public int onUpdate(int updateSize);
    
    public int onComplete(int count);
}
