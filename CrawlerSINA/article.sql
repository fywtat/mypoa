CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `key_word` varchar(100) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` longtext,
  `src_html` longtext,
  `create_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='文章'