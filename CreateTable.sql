CREATE TABLE `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='关键词';

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `key_word` varchar(100) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` longtext,
  `src_html` longtext,
  `create_time` varchar(100) DEFAULT NULL,
  `qgqx` varchar(45) DEFAULT NULL COMMENT '情感倾向：正面、负面、中性',
  `qgqd` varchar(45) DEFAULT NULL COMMENT '情感强度',
  `full_content` longtext,
  `full_html` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7988 DEFAULT CHARSET=utf8 COMMENT='文章';